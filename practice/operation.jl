import Printf


x = Array{Float64}([1,2,3]);
Printf.@printf("x::%s=%s \n", typeof(x), x);

y = x.^ 3;
Printf.@printf("y::%s=%s \n", typeof(y), y);

z = round(Int32, 127.55);
Printf.@printf("z::%s=%s \n", typeof(z), z);
