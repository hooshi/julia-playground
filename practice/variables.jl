import Printf

Printf.@printf("%s \n", typeof(1));

x = Float64(20);
y=  2x^2 - 3x + 1;

Printf.@printf("type(y)=%s y=%f \n", typeof(y), y);

